const express = require('express');
const router = express.Router();

const model = require('../models/index');

/* localhost:5000/api/v1/orders/create */
router.post('/create', async function(req, res, next) {
  const { products } = req.body;
  const user_id = req.user.user_id;

  //บันทึกลงตาราง orders
  const newOrder = await model.Order.create({
    user_id: user_id,
    products: products
  });

  return res.status(201).json({
    message: 'เพิ่มข้อมูลการสั่งซื้อสำเร็จ',
    order: newOrder
  });

});

module.exports = router;
